// select elements
const clear = document.querySelector(".clear");
const dateElement = document.getElementById("date");
const list = document.getElementById("list");
const input = document.getElementById("input");

// classes names
const CHECK = "fa-check-circle";
const UNCHECK = "fa-circle-thin";
const LINE_THROUGH = "lineThrough";

// variables
let LIST ; //array of objects to save the todo added
let id ; // id for each task in the todo list
let data = localStorage.getItem("TODO"); // restore our list array
// let options = { weekday: 'long', month: 'short', day: 'numeric' };
// let today = new Date();


// show the date
let options = { weekday: 'long', month: 'short', day: 'numeric' };
let today = new Date();
dateElement.innerHTML = today.toLocaleDateString('en-US', options);



 if(data){
 	LIST= JSON.parse(data);
 	id = LIST.length;
 	loadList(LIST);  // we load the list to the page

 }else{
 	// if data isn't empty
 	LIST= [];
 	id= 0;
 }


list.addEventListener("click",function(event){
	let element = event.target; //returns the clicked element inside list
	const elementJOB = element.attributes.job.value; // delete or complete
	if (elementJOB == "complete") {
		completeTODO(element);
	}else if(elementJOB == "delete"){
		removeTODO(element);
	}
	localStorage.setItem("TODO",JSON.stringify(LIST)); // TO STORE

});
// for refresh button
clear.addEventListener('click',function(){
	localStorage.clear();
	location.reload();
});

// // to set the date element
// dateElement.innerHTML = today.toLocaleDateString('en-US', options);


function loadList(array){
	array.forEach(function(item){
		addToDo(item.name, item.id, item.done, item.trash);
	});
}



function addToDo(todo, id, done, trash){

	if(trash){return;} // if trash is true which means the task deleted so exit the function

	const DONE = done ? CHECK:UNCHECK; //to decide if task is completed or not and then select class name to apply
	const LINE = done? LINE_THROUGH:"";
	const text =`<li class="item">
					<i class="fa ${DONE} co" job="complete" id="${id}"></i>
					<p class="text ${LINE}">${todo}</p>
					<i class="fa fa-trash-o de" job="delete" id="${id}" ></i>
				</li>`

	const position="beforeend";
	list.insertAdjacentHTML(position,text);
}

document.addEventListener("keyup",function(event){
	if(event.keyCode==13){
		const todo=input.value;
		// to check that todo is not emty
		if(todo){
			addToDo(todo, id, false, false);
			LIST.push(
				{
					name: todo,
					id: id,
					done: false,
					trash: false
				}

				);
			localStorage.setItem("TODO",JSON.stringify(LIST)); // TO STORE
			id++;
		}



		input.value="";


}
});


function completeTODO(element){
	element.classList.toggle(CHECK);
	element.classList.toggle(UNCHECK);
	element.parentNode.querySelector(".text").classList.toggle(LINE_THROUGH);
	// to update our done property in list if it is true so set it to false and vice verse
	LIST[element.id].done = LIST[element.id].done ? false : true;

}

function removeTODO(element){
	element.parentNode.parentNode.removeChild(element.parentNode);
	LIST[element.id].trash = true;

}